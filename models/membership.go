package models

import (
	"time"

	"gorm.io/gorm"
)

type Membership struct {
	BaseModel
	MembershipType string `form:"membership_type" json:"membership_type" validate:"required"` // #enum: Inactive, Basic, Contributing, Sustaining, Lifetime, Commercial, Exchange, Family, Gratis.
	Address1       string `form:"address1" json:"address1" validate:"required"`
	Address2       string `form:"address2" json:"address2"`
	City           string `form:"city" json:"city" validate:"required"`
	State          string `form:"state" json:"state" validate:"required"`
	Zip            string `form:"zip" json:"zipcode" validate:"required"`
	Landline       string `form:"landline" json:"landline"`
	Email          string `form:"email" json:"email"`
	Phone          string `form:"phone" json:"phone"`
	Phone2         string `form:"phone2" json:"phone2"`
	Phone3         string `form:"phone3" json:"phone3"`
	Members        []Member
	Notes          []Note    `gorm:"polymorphic:Owner;" form:"notes" json:"notes"`
	JoinDate       time.Time `form:"join_date" json:"join_date"`   // moment()
	Expiration     time.Time `form:"expiration" json:"expiration"` // moment().add(1, 'years')
}

type ImportMemberships struct {
	Ships []Membership `json:"memberships"`
}

var MembershipTypes []string = []string{"Basic", "Contributing", "Sustaining", "Lifetime", "Commercial", "Exchange", "Family", "Gratis", "Inactive"}

func (m *Membership) BeforeCreate(db *gorm.DB) error {
	var emptyTime time.Time
	if m.JoinDate == emptyTime {
		m.JoinDate = time.Now()
	}
	if m.Expiration == emptyTime {
		m.Expiration = endOfMonthInAYear(time.Now())
	}
	return nil
}

func (membership *Membership) MembershipYearlyRenewal(db *gorm.DB) bool {
	membership.Expiration = endOfMonthInAYear(membership.Expiration)
	result := db.Save(&membership)
	return result.Error == nil
}

func (membership *Membership) MembershipExpireNow(db *gorm.DB) bool {
	membership.Expiration = time.Now()
	result := db.Save(&membership)
	return result.Error == nil
}

func getFirstById(id uint, db *gorm.DB) Membership {
	var membership Membership
	db.Preload("Members").First(&membership, id)
	return membership
}

func EnsureMembershipHasMembers(id uint, db *gorm.DB) bool {
	membership := getFirstById(id, db)
	if len(membership.Members) == 0 {
		db.Delete(&membership)
		return false
	} else {
		return true
	}
}

func endOfMonthInAYear(t time.Time) time.Time {
	if t.Before(time.Now()) {
		t = time.Now()
	}
	return t.AddDate(1, 1, -t.Day())
}

func MembershipHeaders() []string {
	return []string{"membership_type", "address1", "address2", "city", "state", "zip", "landline", "email", "phone", "phone2", "phone3", "join_date", "expiration"}
}

func (m *Membership) ToSlice() []string {
	return []string{
		m.MembershipType,
		m.Address1,
		m.Address2,
		m.City,
		m.State,
		m.Zip,
		m.Landline,
		m.Email,
		m.Phone,
		m.Phone2,
		m.Phone3,
		time.Time(m.JoinDate).Format(time.DateOnly),
		time.Time(m.Expiration).Format(time.DateOnly),
	}
}
