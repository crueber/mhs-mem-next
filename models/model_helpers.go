package models

import (
	"github.com/flamego/flamego"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func ListAll[M Model](db *gorm.DB) []M {
	var hydrated []M
	db.Find(&hydrated)
	return hydrated
}

func PaginatedList[M Model](model *M, paging Pagination, db *gorm.DB) ([]M, Pagination) {
	var hydrated []M
	var count int64

	db.Scopes(paging.SearchFn).Limit(paging.PerPage).Offset(paging.Offset()).Preload(clause.Associations).Find(&hydrated)
	db.Model(model).Scopes(paging.SearchFn).Count(&count)
	paging.SetTotal(count)
	return hydrated, paging
}

func Get[M Model](id int, db *gorm.DB) M {
	var hydrated M
	db.Preload(clause.Associations).First(&hydrated, id)
	return hydrated
}

func Create[M Model](model *M, db *gorm.DB) error {
	result := db.Create(&model)
	return result.Error
}

func Update[M Model, PT IDer[M]](c flamego.Context, paramId string, fields []string, db *gorm.DB) M {
	updates := map[string]interface{}{}
	params := c.Request().Form
	for _, field := range fields {
		updates[field] = params[field]
	}

	id := c.ParamInt(paramId)
	model := PT(new(M))
	model.SetID(uint(id))

	db.Model(model).Updates(updates)
	return Get[M](id, db)
}

func Delete[M Model](model M, db *gorm.DB) error {
	result := db.Delete(&model)
	return result.Error
}

func exists(db *gorm.DB) bool {
	var count int64
	db.Count(&count)
	return count > 0
}
