package models

type Note struct {
	BaseModel
	OwnerID   uint
	OwnerType string
	Note      string `form:"note" json:"note" validate:"required"`
}
