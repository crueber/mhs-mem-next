package models

import (
	"time"

	"gorm.io/gorm"
)

type BaseModel struct {
	ID        uint           `gorm:"primaryKey,autoIncrement" json:"id"`
	CreatedAt time.Time      `form:"created_at" json:"created_at"`
	UpdatedAt time.Time      `form:"updated_at" json:"updated_at"`
	DeletedAt gorm.DeletedAt `gorm:"index" form:"deleted_at" json:"deleted_at"`
}

func (m *BaseModel) SetID(id uint) {
	m.ID = id
}

func (m *BaseModel) BeforeCreate(db *gorm.DB) error {
	m.CreatedAt = time.Now()
	return nil
}

func (m *BaseModel) BeforeUpdate(db *gorm.DB) error {
	m.UpdatedAt = time.Now()
	return nil
}
