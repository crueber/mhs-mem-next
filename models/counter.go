package models

import (
	"gorm.io/gorm"
)

type Counter struct {
	Name   string `gorm:"primaryKey"`
	Number uint
}

func counterNext(name string, db *gorm.DB) uint {
	var counter Counter
	db.Where("name = ?", name).First(&counter)

	counter.Number += 1
	if counter.Name == "" {
		counter.Name = name
	}

	db.Save(&counter)
	return counter.Number
}

func ensureCounterMin(name string, min uint, db *gorm.DB) {
	var counter Counter
	db.Select("max(number) as number, name").Where("name = ?", name).First(&counter)
	if min <= counter.Number {
		counter.Number = min
		db.Save(&counter)
	}
}
