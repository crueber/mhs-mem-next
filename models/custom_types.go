package models

import (
	"fmt"
	"time"
)

type DateTime time.Time

func (t *DateTime) UnmarshalJSON(b []byte) (err error) {
	s := string(b)
	if s != "" && s[0] == '"' && s[len(s)-1] == '"' {
		s = s[1 : len(s)-1]
	}

	tt, err := time.Parse(time.DateOnly, s)
	if err == nil {
		*t = DateTime(tt)
		return nil
	}

	return fmt.Errorf("invalid duration type %T, value: '%s'", b, b)
}
