package models

import (
	"strconv"

	"gorm.io/gorm"
)

type Member struct {
	BaseModel
	MembershipID uint `form:"membership" json:"membership"`
	Membership   Membership
	MHSID        uint   `form:"mhs_id" json:"mhs_id"`
	FirstName    string `form:"first_name" json:"first_name" validate:"required"`
	LastName     string `form:"last_name" json:"last_name" validate:"required"`
	Email        string `form:"email" json:"email"`
	Phone        string `form:"phone" json:"phone"`
	Notes        []Note `gorm:"polymorphic:Owner;" form:"notes" json:"notes"`
}

func (m *Member) BeforeCreate(db *gorm.DB) error {
	if m.MHSID == 0 {
		m.MHSID = assignMHSID(db)
	}
	return nil
}

const counterName string = "MHSID"

func assignMHSID(db *gorm.DB) uint {
	var mem Member
	db.Unscoped().Select("MAX(mhs_id) as mhs_id").First(&mem)
	max := mem.MHSID
	ensureCounterMin(counterName, max+1, db)

	id := counterNext(counterName, db)
	if exists(db.Where("mhs_id = ?", id)) {
		return assignMHSID(db)
	}
	return id
}

func MemberHeaders() []string {
	return []string{"id", "membership_id", "mhs_id", "first_name", "last_name", "email", "phone"}
}

func (m *Member) ToSlice() []string {
	return []string{
		strconv.Itoa(int(m.ID)),
		strconv.Itoa(int(m.MembershipID)),
		strconv.Itoa(int(m.MHSID)),
		m.FirstName,
		m.LastName,
		m.Email,
		m.Phone,
	}
}
