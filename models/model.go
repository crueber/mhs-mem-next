package models

type Model interface {
	Member | Membership | Note
}

type IDer[M Member | Membership | Note] interface {
	SetID(uint)
	*M
}
