package models

import (
	"github.com/flamego/flamego"
	"gorm.io/gorm"
)

type Search func(*gorm.DB) *gorm.DB
type Pagination struct {
	TotalPages  int64
	CurrentPage int64
	PerPage     int
	URI         string
	PrevPage    int64
	NextPage    int64
	Context     map[string]string
	SearchFn    Search
}

func SetPagination(c flamego.Context, uri string) Pagination {
	page := c.QueryInt64("page")
	per := c.QueryInt("perpage")

	if page == 0 {
		page = 1
	}
	if per == 0 {
		per = 20
	}
	paging := Pagination{TotalPages: 0, CurrentPage: page, PerPage: per, URI: uri, PrevPage: 0, NextPage: 0, Context: map[string]string{}}
	paging.Context["search"] = c.QueryTrim("search")
	paging.Context["context"] = c.QueryTrim("context")

	return paging
}

func (p *Pagination) Offset() int {
	return int((p.CurrentPage - 1) * int64(p.PerPage))
}

func (p *Pagination) SetTotal(count int64) {
	p.TotalPages = (count / int64(p.PerPage)) + 1

	if p.CurrentPage > 1 {
		p.PrevPage = p.CurrentPage - 1
	} else {
		p.PrevPage = 0
	}

	if p.CurrentPage < p.TotalPages {
		p.NextPage = p.CurrentPage + 1
	} else {
		p.NextPage = 0
	}
}
