package main

import (
	"log"
	"os"
	"time"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"

	m "mhs-db/models"
)

func OpenDatabase() *gorm.DB {
	newLogger := logger.New(
		log.New(os.Stdout, "\r", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Second, // Slow SQL threshold
			LogLevel:                  logger.Info, // Log level
			IgnoreRecordNotFoundError: true,        // Ignore ErrRecordNotFound error for logger
			ParameterizedQueries:      true,        // Don't include params in the SQL log
			Colorful:                  true,        // Disable color
		},
	)

	db, err := gorm.Open(sqlite.Open("sqlite.db"), &gorm.Config{Logger: newLogger})
	if err != nil {
		panic("Unable to open sqlite db: sqlite.db")
	}

	db.AutoMigrate(
		&m.Membership{},
		&m.Member{},
		&m.Note{},
		&m.Counter{},
	)

	return db
}
