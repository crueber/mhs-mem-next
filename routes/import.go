package routes

import (
	"encoding/json"
	"fmt"
	"io"
	m "mhs-db/models"
	"net/http"

	"github.com/flamego/flamego"
	"gorm.io/gorm"
)

func MembershipImport(c flamego.Context, db *gorm.DB) {
	importShips := m.ImportMemberships{
		Ships: []m.Membership{},
	}

	dec := json.NewDecoder(c.Request().Request.Body)
	err := db.Transaction(func(tx *gorm.DB) error {

		for {
			if err := dec.Decode(&importShips); err == io.EOF {
				break
			} else if err != nil {
				fmt.Println(err)
				break
			}
		}

		for _, ship := range importShips.Ships {
			if err := tx.Create(&ship).Error; err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		fmt.Println(err)
	}
	c.Redirect("/members", http.StatusFound)
}
