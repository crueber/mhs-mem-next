package routes

import (
	"fmt"
	m "mhs-db/models"
	"net/http"
	"strconv"
	"time"

	"github.com/flamego/flamego"
	"github.com/flamego/template"
	"gorm.io/gorm"
)

func MembershipNew(t template.Template, d template.Data) {
	d["MembershipTypes"] = m.MembershipTypes
	t.HTML(http.StatusOK, "membership/create")
}

func MembershipCreate(c flamego.Context, t template.Template, d template.Data, member m.Member, membership m.Membership, db *gorm.DB) {
	member.Membership = membership
	m.Create(&member, db)
	c.Redirect(fmt.Sprintf("%s%d", "/membership/", member.MembershipID), http.StatusFound)
}

func MembershipRead(c flamego.Context, t template.Template, d template.Data, db *gorm.DB) {
	d["Membership"] = m.Get[m.Membership](c.ParamInt("membership_id"), db)
	d["Now"] = time.Now()
	t.HTML(http.StatusOK, "membership/read")
}

func MembershipEdit(c flamego.Context, t template.Template, d template.Data, db *gorm.DB) {
	d["MembershipTypes"] = m.MembershipTypes
	d["Membership"] = m.Get[m.Membership](c.ParamInt("membership_id"), db)
	t.HTML(http.StatusOK, "membership/edit-membership")
}
func MembershipUpdate(c flamego.Context, t template.Template, d template.Data, db *gorm.DB) {
	fields := []string{"membership_type", "address1", "address2", "city", "state", "zip", "landline", "email", "phone"}
	d["Membership"] = m.Update[m.Membership](c, "membership_id", fields, db)
	t.HTML(http.StatusAccepted, "membership/read-membership")
}

func MembershipMemberCreate(c flamego.Context, t template.Template, d template.Data, member m.Member, db *gorm.DB) {
	member.MembershipID = uint(c.ParamInt("membership_id"))
	membership := m.Get[m.Membership](c.ParamInt("membership_id"), db)
	db.Model(&membership).Association("Members").Append(&member)
	d["Member"] = member
	t.HTML(http.StatusAccepted, "membership/read-member")
}

func MembershipMemberEdit(c flamego.Context, t template.Template, d template.Data, db *gorm.DB) {
	d["MembershipID"] = c.ParamInt("membership_id")
	d["Member"] = m.Get[m.Member](c.ParamInt("member_id"), db)
	t.HTML(http.StatusOK, "membership/edit-member")
}

func MembershipMemberTransfer(c flamego.Context, t template.Template, d template.Data, db *gorm.DB) {
	var members []m.Member
	db.Select("first_name", "last_name", "id", "membership_id").Order("last_name asc").Not("membership_id = ?", c.Param("membership_id")).Find(&members)
	d["Members"] = members
	d["Member"] = m.Get[m.Member](c.ParamInt("member_id"), db)
	d["MembershipTypes"] = m.MembershipTypes
	t.HTML(http.StatusOK, "membership/transfer-member")
}

func MembershipMemberTransferComplete(c flamego.Context, db *gorm.DB) {
	c.Request().Request.ParseForm()
	memberId := c.ParamInt("member_id")
	transferId := c.Request().Form["transfer"][0]
	membershipIdTo, _ := strconv.Atoi(transferId)
	member := m.Get[m.Member](memberId, db)
	membershipTo := m.Get[m.Membership](membershipIdTo, db)
	db.Model(&member).Association("Membership").Replace(&membershipTo)

	m.EnsureMembershipHasMembers(uint(c.ParamInt("membership_id")), db)

	HXRedirect("/membership/"+transferId, "body", c)
	Accepted(c)
}

func MembershipMemberToNewMembership(c flamego.Context, membership m.Membership, db *gorm.DB) {
	member := m.Get[m.Member](c.ParamInt("member_id"), db)
	member.Membership = membership
	db.Save(&member)

	m.EnsureMembershipHasMembers(uint(c.ParamInt("membership_id")), db)

	membershipId := strconv.Itoa(int(member.Membership.ID))
	HXRedirect("/membership/"+membershipId, "body", c)
	Accepted(c)
}

func MembershipMemberDelete(c flamego.Context, db *gorm.DB) {
	member := m.Get[m.Member](c.ParamInt("member_id"), db)
	m.Delete(member, db)

	if m.EnsureMembershipHasMembers(uint(c.ParamInt("membership_id")), db) {
		HXRedirect("/membership/"+c.Param("membership_id"), "body", c)
	} else {
		HXRedirect("/members", "body", c)
	}

	Accepted(c)
}

func MembershipMemberUpdate(c flamego.Context, t template.Template, d template.Data, db *gorm.DB) {
	fields := []string{"first_name", "last_name", "email", "phone"}
	member := m.Update[m.Member](c, "member_id", fields, db)

	d["Members"] = m.Get[m.Membership](int(member.MembershipID), db).Members
	t.HTML(http.StatusAccepted, "membership/read-partial-member")
}

func MembershipExpireNow(c flamego.Context, t template.Template, d template.Data, db *gorm.DB) {
	membership := m.Get[m.Membership](c.ParamInt("membership_id"), db)
	membership.MembershipExpireNow(db)
	d["Membership"] = membership
	t.HTML(http.StatusOK, "membership/read-membership")
}

func MembershipRenewal(c flamego.Context, t template.Template, d template.Data, db *gorm.DB) {
	membership := m.Get[m.Membership](c.ParamInt("membership_id"), db)
	membership.MembershipYearlyRenewal(db)
	d["Membership"] = membership
	t.HTML(http.StatusOK, "membership/read-membership")
}
