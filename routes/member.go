package routes

import (
	"net/http"
	"time"

	"github.com/flamego/binding"
	"github.com/flamego/flamego"
	"github.com/flamego/template"
	"gorm.io/gorm"

	m "mhs-db/models"
)

func MemberSearchScope(paging m.Pagination) func(db *gorm.DB) *gorm.DB {
	search := paging.Context["search"]
	context := paging.Context["context"] // active, expired, all

	return func(db *gorm.DB) *gorm.DB {
		db.Joins("Membership")
		if search != "" {
			search = "%" + search + "%"
			db.Where("first_name LIKE ?", search).Or("last_name LIKE ?", search).Or("members.email LIKE ?", search)
		}
		if context == "active" || context == "" {
			db.Order("membership.expiration asc").Where("membership.expiration > ?", time.Now())
		}
		if context == "expired" {
			db.Order("membership.expiration desc").Where("membership.expiration <= ?", time.Now())
		}
		if context == "all" {
			db.Order("mhs_id asc")
		}
		return db
	}
}

func MemberIndex(c flamego.Context, t template.Template, d template.Data, db *gorm.DB) {
	paging := m.SetPagination(c, "/members.partial")
	paging.SearchFn = MemberSearchScope(paging)
	d["Members"], d["Pagination"] = m.PaginatedList(&m.Member{}, paging, db)
	t.HTML(http.StatusOK, "member/index")
}

func MemberIndexPartial(c flamego.Context, t template.Template, d template.Data, db *gorm.DB) {
	paging := m.SetPagination(c, "/members.partial")
	paging.SearchFn = MemberSearchScope(paging)
	d["Members"], d["Pagination"] = m.PaginatedList(&m.Member{}, paging, db)
	t.HTML(http.StatusOK, "member/index-partial")
}

func MemberRead(c flamego.Context, t template.Template, d template.Data, db *gorm.DB) {
	d["Member"] = m.Get[m.Member](c.ParamInt("id"), db)
	t.HTML(http.StatusOK, "member/read")
}

func MemberUpdate(c flamego.Context, t template.Template, d template.Data, memberUpdates m.Member, db *gorm.DB, errs binding.Errors) {
	fields := []string{"first_name", "last_name", "email", "phone"}
	member := m.Update[m.Member](c, "member_id", fields, db)

	d["Members"] = m.Get[m.Membership](int(member.MembershipID), db).Members
	t.HTML(http.StatusAccepted, "membership/read-partial-member")
}

func MemberCreate(c flamego.Context, t template.Template, d template.Data, member m.Member, db *gorm.DB, errs binding.Errors) {
	m.Create(&member, db)
	d["Member"] = member
	t.HTML(http.StatusCreated, "member/read-partial")
}

func MemberDelete(c flamego.Context, db *gorm.DB) {
	member := m.Get[m.Member](c.ParamInt("id"), db)
	m.Delete(member, db)
	if c.Request().Method == http.MethodGet {
		c.Redirect("/members", http.StatusFound)
	} else {
		StatusOK(c)
	}
}
