package routes

import (
	"encoding/csv"
	m "mhs-db/models"
	"net/http"
	"time"

	"github.com/flamego/flamego"
	"github.com/flamego/template"
	"gorm.io/gorm"
)

func Index(c flamego.Context, t template.Template, d template.Data, db *gorm.DB) {
	var activeMemberships, totalMembers, allMembers int64
	db.Model(&m.Membership{}).Where("expiration > ?", time.Now()).Count(&activeMemberships)
	d["ActiveMemberships"] = activeMemberships

	var ids []uint
	db.Model(&m.Membership{}).Where("expiration > ?", time.Now()).Pluck("id", &ids)
	db.Model(&m.Member{}).Where("membership_id IN ?", ids).Count(&totalMembers)
	d["TotalMembers"] = totalMembers

	db.Model(&m.Member{}).Count(&allMembers)
	d["AllMembers"] = allMembers

	t.HTML(http.StatusOK, "index")
}

func CSVMembers(c flamego.Context, t template.Template, d template.Data, db *gorm.DB) {
	rw := c.ResponseWriter()
	rw.Header().Add("Content-Disposition", "attachment; filename=members.csv")
	rw.Header().Add("Content-type", "application/vnd.ms-excel")
	rw.Header().Add("Cache-Control", "must-revalidate")
	rw.Header().Add("Pragma", "must-revalidate")

	writer := csv.NewWriter(rw)
	var ships []m.Membership

	if c.Query("include") != "all" {
		db = db.Where("expiration > ?", time.Now())
	}
	db.Preload("Members").FindInBatches(&ships, 20, func(tx *gorm.DB, batch int) error {
		if batch == 1 {
			writer.Write(append(m.MemberHeaders(), m.MembershipHeaders()...))
		}
		for _, ship := range ships {
			for _, member := range ship.Members {
				writer.Write(append(member.ToSlice(), ship.ToSlice()...))
			}
		}
		return nil
	})
	writer.Flush()
	rw.Flush()
}
