package routes

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/flamego/binding"
	"github.com/flamego/flamego"
	"github.com/flamego/validator"
)

func StatusOK(c flamego.Context) {
	w := c.ResponseWriter()
	w.WriteHeader(http.StatusOK)
	w.Write([]byte{})
}

func Accepted(c flamego.Context) {
	w := c.ResponseWriter()
	w.WriteHeader(http.StatusAccepted)
	w.Write([]byte{})
}

func HXRedirect(uri, domLocation string, c flamego.Context) {
	w := c.ResponseWriter()

	hxlocation := map[string]string{
		"path":   uri,
		"target": domLocation,
	}
	header, _ := json.Marshal(hxlocation)
	w.Header().Add("HX-Location", string(header[:]))
}

func ValidationHandler(c flamego.Context, errs binding.Errors) {
	if len(errs) == 0 {
		c.Next()
	} else {
		var err error
		switch errs[0].Category {
		case binding.ErrorCategoryValidation:
			err = errs[0].Err.(validator.ValidationErrors)[0]
		default:
			err = errs[0].Err
		}

		w := c.ResponseWriter()
		w.WriteHeader(http.StatusBadRequest)
		_, _ = w.Write([]byte(fmt.Sprintf("Error: %v", err)))
	}
}
