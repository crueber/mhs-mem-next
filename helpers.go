package main

import (
	"html/template"
	"strconv"
	"strings"
	"time"

	"github.com/dustin/go-humanize"
)

func Helpers() template.FuncMap {
	return template.FuncMap{
		"RelativeTime":     RelativeTime,
		"DateOnly":         DateOnly,
		"Add":              Add,
		"BuildPagingURI":   BuildPagingURI,
		"gtNow":            gtNow,
		"NumberOfYearsAgo": NumberOfYearsAgo,
	}
}

func NumberOfYearsAgo(t time.Time) int {
	return time.Now().Year() - t.Year()
}

func gtNow(t time.Time) bool {
	return time.Now().Compare(t) > 0
}

func DateOnly(t time.Time) string {
	return t.Format("01/02/2006")
}

func RelativeTime(t time.Time) string {
	return humanize.Time(t)
}

func Add(n int64, a int64) int64 {
	return n + a
}

func BuildPagingURI(baseuri string, page int64, perpage int, context map[string]string) string {
	var uri strings.Builder
	uri.WriteString(baseuri)
	uri.WriteString("?")
	if page != 0 {
		uri.WriteString("page=")
		uri.WriteString(strconv.FormatInt(page, 10))
	}
	if perpage != 5 && perpage != 0 {
		uri.WriteString("&perpage=")
		uri.WriteString(strconv.Itoa(perpage))
	}
	if context["search"] != "" {
		uri.WriteString("&search=")
		uri.WriteString(context["search"])
	}
	if context["context"] != "" {
		uri.WriteString("&context=")
		uri.WriteString(context["context"])
	}
	return uri.String()
}
