package main

import (
	t "html/template"
	"os"
	"os/signal"
	"syscall"

	"github.com/flamego/binding"
	"github.com/flamego/flamego"
	"github.com/flamego/session"
	"github.com/flamego/template"
	"gorm.io/gorm"

	m "mhs-db/models"
	r "mhs-db/routes"
	"mhs-db/templates"
)

func main() {
	fs, _ := template.EmbedFS(templates.Templates, ".", []string{".tmpl"})
	db := OpenDatabase()
	f := flamego.New()
	go processWatcher(db, f)

	f.Map(db)
	f.Use(flamego.Logger())
	f.Use(flamego.Recovery())
	f.Use(flamego.Static(flamego.StaticOptions{Directory: "public"}))
	f.Use(template.Templater(template.Options{FuncMaps: []t.FuncMap{Helpers()}, FileSystem: fs}))
	f.Use(flamego.Renderer(flamego.RenderOptions{JSONIndent: "  "}))
	f.Use(HXBoosted)
	f.Use(session.Sessioner())

	f.Get("/", r.Index)
	f.Get("/members.csv", r.CSVMembers)
	f.Post("/member", binding.Form(m.Member{}), r.ValidationHandler, r.MemberCreate)
	f.Get("/members", r.MemberIndex)
	f.Get("/members.partial", r.MemberIndexPartial)
	f.Get("/member/{member_id}", r.MemberRead)
	f.Delete("/member/{member_id}", r.MemberDelete)
	f.Get("/member/{member_id}/delete", r.MemberDelete)

	f.Get("/membership/new", r.MembershipNew)
	f.Post("/membership/import", r.MembershipImport)
	f.Post("/membership", binding.Form(m.Member{}), binding.Form(m.Membership{}), r.ValidationHandler, r.MembershipCreate)
	f.Group("/membership/{membership_id}", func() {
		f.Get("", r.MembershipRead)
		f.Put("", binding.Form(m.Membership{}), r.ValidationHandler, r.MembershipUpdate)
		f.Get("/edit", r.MembershipEdit)
		f.Put("/expire", r.MembershipExpireNow)
		f.Put("/renew", r.MembershipRenewal)
		f.Post("/member", binding.Form(m.Member{}), r.MembershipMemberCreate)

		f.Get("/member/{member_id}/edit", r.MembershipMemberEdit)
		f.Get("/member/{member_id}/transfer", r.MembershipMemberTransfer)
		f.Put("/member/{member_id}/transfer", r.MembershipMemberTransferComplete)
		f.Put("/member/{member_id}/promote", binding.Form(m.Membership{}), r.ValidationHandler, r.MembershipMemberToNewMembership)
		f.Delete("/member/{member_id}", r.MembershipMemberDelete)
		f.Put("/member/{member_id}", binding.Form(m.Member{}), r.MembershipMemberUpdate)
	})

	f.Run()
}

func processWatcher(db *gorm.DB, f *flamego.Flame) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	signal.Notify(c, os.Interrupt, syscall.SIGINT)

	<-c

	sqldb, _ := db.DB()

	sqldb.Close()
	f.Stop()
}
